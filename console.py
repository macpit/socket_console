async_mode = 'eventlet'

import eventlet
eventlet.monkey_patch()

import datetime
from flask import Flask, render_template, session, request, jsonify
from flask_socketio import SocketIO, emit, disconnect

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

def alert(a):
    if a == 'success':
        return 'alert-success alert-dismissible'
    elif a == 'warning':
        return 'alert-warning alert-dismissible'
    elif a == 'danger':
        return 'alert-danger alert-dismissible'
    else:
        return 'alert-info alert-dismissible'

@app.route('/')
def index():
    return render_template('console.html')

# curl --data "severity=danger&msg=üöäàéè" http://10.0.2.202:5000/msg
@app.route('/msg', methods=['POST'])
def _cmd():
    lt = str(datetime.datetime.now()) + '<br>'
    socketio.emit('my response', {'data': lt + request.form['msg'], 'class': alert(request.form['severity'])}, namespace='/test')
    return jsonify({"value": 'OK'})


@socketio.on('my event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.on('my broadcast event', namespace='/test')
def test_broadcast_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)


@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected', 'class': alert('info')})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, host='192.168.21.200', port=8080)
