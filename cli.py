import urllib.request, urllib.parse

class console():

    def __init__(self, ip, port):
        self._ip = ip
        self._port = port

    def send(self, severity, msg):
        data = {'severity': severity, 'msg': msg}
        data = bytes(urllib.parse.urlencode(data).encode())
        try:
            urllib.request.urlopen('http://' + self._ip + ':' + self._port + '/msg', data, timeout=0.2)
        except:
            print('Error send msg')

    def success(self, msg):
        self.send('success', msg)

    def warning(self, msg):
        self.send('warning', msg)

    def danger(self, msg):
        self.send('danger', msg)

    def info(self, msg):
        self.send('info', msg)

if __name__ == '__main__':
    alert = console('0.0.0.0', '8080')

    alert.danger('Danger <br> Alarm 1 OG')
    alert.warning('Warning Test')
    alert.link('http://192.168.2.254', 'Paessler')
    alert.success('Success Backup done')
    alert.img('http://www.zerohedge.com/sites/default/files/images/user5/imageroot/2015/08/burnt%20server.jpg')
    alert.img('http://www.gifs.net/Animation11/Creatures_and_Cartoons/Cartoons_Simpsons/Bart_and_Lisa.gif')
    alert.img('http://static4.businessinsider.com/image/4f306aaeecad04631f00005f-506-253/cisco-warning-some-of-our-servers-may-burn-out.jpg')
    alert.danger('<font size="30">Stromausfall</font>')
